![Logo](./../figs/RATLogo.png)
# Installation sequence for Rat in Windows
<em>Rat says, windows stinks ...</em>

git@gitlab.com:Project-Rat/rat-documentation.git


## Windows 
download vcpkg
./bootstrap-vcpkg.bat
./vcpkg install


./vcpkg install armadillo[core] // single threaded version


Add vcpkg\installed\x64-windows\bin directory to system search path

set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)

Developer Powershell for VS2019

make sure to build openblas with [simplethreads]
https://github.com/microsoft/vcpkg/issues/15698

Find cuda in vcpkg (requires little hack)
https://github.com/Microsoft/vcpkg/issues/3609

cd ../../rat-common/build & ninja & ninja install & cd ../../distmesh-cpp/build & ninja & ninja install & cd ../../materials-cp/build & ninja & ninja install & cd ../../rat-mlfmm/build & ninja & ninja install & cd ../../rat-models/build & ninja & ninja install & cd ../../rat-gui/build & ninja & .\Release\bin\gui

# compilation of suitesparse
modify SuiteSparseLapack.cmake
find_package(BLAS)
find_package(LAPACK)

cmake .. -G "Ninja" -DNGPL & cmake --build . --config Release

cmake .. -G "Ninja" -DNMATRIXOPS=true -DNMODIFY=true -DNSUPERNODAL=true -DENABLE_CUDA=false -DCMAKE_BUILD_TYPE=Release  & cmake --build . --config Release

cmake .. -G "Ninja" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" & cmake --build . --config Release


# compilation of rat
cd ../../rat-common & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DBLA_VENDOR=OpenBLAS -DVCPKG_TARGET_TRIPLET=x64-windows -DBLAS_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\openblas.lib" -DLAPACK_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\lapack.lib" & ninja & ninja install & cd ../../distmesh-cpp & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DBLA_VENDOR=OpenBLAS -DVCPKG_TARGET_TRIPLET=x64-windows -DBLAS_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\openblas.lib" -DLAPACK_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\lapack.lib" & ninja & ninja install & cd ../../materials-cpp & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DBLA_VENDOR=OpenBLAS -DVCPKG_TARGET_TRIPLET=x64-windows -DBLAS_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\openblas.lib" -DLAPACK_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\lapack.lib" & ninja & ninja install & cd ../../rat-mlfmm & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DBLA_VENDOR=OpenBLAS -DVCPKG_TARGET_TRIPLET=x64-windows -DBLAS_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\openblas.lib" -DLAPACK_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\lapack.lib" & ninja & ninja install & cd ../../rat-models & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DBLA_VENDOR=OpenBLAS -DVCPKG_TARGET_TRIPLET=x64-windows -DBLAS_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\openblas.lib" -DLAPACK_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\lapack.lib" & ninja & ninja install & cd ../../rat-gui & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DBLA_VENDOR=OpenBLAS -DVCPKG_TARGET_TRIPLET=x64-windows -DBLAS_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\openblas.lib" -DLAPACK_LIBRARIES="C:\dev\vcpkg\installed\x64-windows\lib\lapack.lib" & ninja



cd ../../rat-common & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows  & ninja & ninja install & cd ../../distmesh-cpp & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows  & ninja & ninja install & cd ../../materials-cpp & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows  & ninja & ninja install & cd ../../rat-mlfmm & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows  & ninja & ninja install & cd ../../rat-nl & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows  & ninja & ninja install & cd ../../rat-models & rmdir /S/Q build & mkdir build & cd build & cmake .. -DENABLE_NL_SOLVER=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows  & ninja & ninja install & cd ../../rat-gui & rmdir /S/Q build & mkdir build & cd build & cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DCMAKE_INSTALL_PREFIX="C:\dev\vcpkg\installed\x64-windows" -DVCPKG_TARGET_TRIPLET=x64-windows & ninja


set(CUDA_HOST_COMPILER clang++)
set(CMAKE_CUDA_COMPILER /usr/bin/clang++)